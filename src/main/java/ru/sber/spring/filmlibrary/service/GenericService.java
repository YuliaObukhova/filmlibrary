package ru.sber.spring.filmlibrary.service;

import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.sber.spring.filmlibrary.dto.GenericDTO;
import ru.sber.spring.filmlibrary.mapper.GenericMapper;
import ru.sber.spring.filmlibrary.model.GenericModel;
import ru.sber.spring.filmlibrary.repository.GenericRepository;

import java.time.LocalDateTime;
import java.util.List;


/**
 *  * Абстрактный сервис, который хранит в себе реализацию CRUD операций по-умолчанию.
 *  * Если реализация отличная от того, что представлено в этом классе,
 *  * то она переопределяется в реализации конкретного сервиса.
 *
  * @param <T> - Сущность, с которой мы работаем.
  * @param <N> - DTO, которую мы будем отдавать/принимать дальше.
 */


@Service
public abstract class GenericService<T extends GenericModel, N extends GenericDTO> {
    protected final GenericRepository<T> repository;
    //Инжектим абстрактный маппер для преобразований из DTO -> Entity, и обратно.
    protected final GenericMapper<T, N> mapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    protected GenericService(GenericRepository<T> repository,
                             GenericMapper<T, N> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    /**
     * Метод, возвращающий полный список всех сущностей.
     *
     * @return Список сконвертированных сущностей в DTO
     */
    public List<N> listAll() {
        return mapper.toDTOs(repository.findAll());
    }

    /***
     * Получить информацию о конкретном объекте/сущности по ID.
     *
     * @param id - идентификатор сущности для поиска.
     * @return - конкретная сущность в формате DTO
     */
    public N getOne(Long id) {
        return mapper.toDTO(repository.findById(id).orElseThrow(() -> new NotFoundException("Данных по заданному id: " + id + " не найдено")));
    }

    /***
     * Создание сущности в БД.
     *
     * @param newObject - информация о сущности/объекте.
     * @return - сохраненная в БД сущность в формате DTO.
     */

    public N create(N newObject) {
        newObject.setCreatedBy("ADMIN");
        newObject.setCreatedWhen(LocalDateTime.now());
        System.out.println(newObject);
        System.out.println(mapper.toEntity(newObject));
        return mapper.toDTO(repository.save(mapper.toEntity(newObject)));
    }

    /***
     * Обновление сущности в БД.
     *
     * @param updatedObject - информация о сущности/объекте.
     * @return - обновленная в БД сущность в формате DTO.
     */
    public N update(N updatedObject) {
        return mapper.toDTO(repository.save(mapper.toEntity(updatedObject)));
    }

    /***
     * Удаление сущности из БД.
     *
     * @param id - идентификатор сущности, которая должна быть удалена.
     */
    public void delete(Long id) {
        repository.deleteById(id);
    }
}

