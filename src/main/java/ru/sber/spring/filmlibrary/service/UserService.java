package ru.sber.spring.filmlibrary.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.sber.spring.filmlibrary.dto.FilmDTO;
import ru.sber.spring.filmlibrary.dto.RoleDTO;
import ru.sber.spring.filmlibrary.dto.UserDTO;
import ru.sber.spring.filmlibrary.mapper.FilmMapper;
import ru.sber.spring.filmlibrary.mapper.UserMapper;
import ru.sber.spring.filmlibrary.model.Order;
import ru.sber.spring.filmlibrary.model.User;
import ru.sber.spring.filmlibrary.repository.UserRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDate;
import java.util.HashSet;
import java.util.Set;

@Service
public class UserService
        extends GenericService<User, UserDTO> {
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private UserRepository userRepository;

    FilmMapper filmMapper;

    protected UserService(UserRepository userRepository,
                          UserMapper userMapper,
                          FilmMapper filmMapper,
                          BCryptPasswordEncoder bCryptPasswordEncoder) {
        super(userRepository, userMapper);
        this.userRepository = userRepository;
        this.filmMapper = filmMapper;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public UserDTO getUserByLogin(final String login) {
        return mapper.toDTO(((UserRepository) repository).findUserByLogin(login));
    }

    public UserDTO getUserByEmail(final String email) {
        return mapper.toDTO(((UserRepository) repository).findUserByEmail(email));
    }

    public Boolean checkPassword(String password, UserDetails userDetails) {
        return bCryptPasswordEncoder.matches(password, userDetails.getPassword());
    }

    @Override
    public UserDTO create(UserDTO object) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(1L);
        object.setRole(roleDTO);
        object.setCreatedBy("REGISTRATION FORM");
        object.setCreatedWhen(LocalDateTime.now());
        object.setPassword(bCryptPasswordEncoder.encode(object.getPassword()));
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }

    public Set<FilmDTO> getFilmList(Long userId) {
        Set<FilmDTO> filmDTOS = new HashSet<>();
        User user = repository.findById(userId)
                .orElseThrow(() -> new NotFoundException("Пользователь с таким ID не найден"));
        Set<Order> orders = user.getOrders();

        LocalDate today = LocalDate.now();
        for (Order order : orders) {
            if (order.isPurchase() ||
                    today.isAfter(ChronoLocalDate.from(order.getCreatedWhen().minusDays(1))) &&
                            today.isBefore(ChronoLocalDate.from(order.getCreatedWhen().plusDays(order.getRentPeriod() + 1)))) {
                filmDTOS.add(filmMapper.toDTO(order.getFilm()));
            }
        }
        return filmDTOS;
    }
}

