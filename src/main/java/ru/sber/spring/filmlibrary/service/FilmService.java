package ru.sber.spring.filmlibrary.service;

import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import ru.sber.spring.filmlibrary.dto.FilmDTO;

import ru.sber.spring.filmlibrary.dto.FilmWithDirectorsDTO;
import ru.sber.spring.filmlibrary.mapper.FilmMapper;
import ru.sber.spring.filmlibrary.mapper.FilmWithDirectorsMapper;
import ru.sber.spring.filmlibrary.model.Director;
import ru.sber.spring.filmlibrary.model.Film;
import ru.sber.spring.filmlibrary.repository.DirectorRepository;
import ru.sber.spring.filmlibrary.repository.FilmRepository;

import java.util.List;

@Service
public class FilmService extends GenericService<Film, FilmDTO> {
    private final FilmRepository filmRepository;
   private final DirectorRepository directorRepository;
   private final FilmWithDirectorsMapper filmWithDirectorsMapper;
    protected FilmService(FilmRepository filmRepository,
                          DirectorRepository directorRepository,
                          FilmMapper mapper,
                          FilmWithDirectorsMapper filmWithDirectorsMapper) {
        super(filmRepository, mapper);
        this.directorRepository = directorRepository;
        this.filmWithDirectorsMapper = filmWithDirectorsMapper;
        this.filmRepository = filmRepository;
    }

    public FilmDTO addDirector(Long filmId, Long directorId){
        Director director = directorRepository.findById(directorId)
                .orElseThrow(() -> new NotFoundException("Директор с id: " + directorId + " не найден"));
        Film film = filmRepository.findById(filmId)
                .orElseThrow(() -> new NotFoundException("Фильм с id: " + filmId + " не найден"));

        film.getDirectors().add(director);
        return mapper.toDTO(repository.save(film));
        }

    public List<FilmWithDirectorsDTO> getAllFilmsWithDirectors() {
        return filmWithDirectorsMapper.toDTOs(filmRepository.findAll());
    }
}
