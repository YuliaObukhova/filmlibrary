package ru.sber.spring.filmlibrary.service;

import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.sber.spring.filmlibrary.dto.DirectorDTO;
import ru.sber.spring.filmlibrary.dto.DirectorWithFilmsDTO;
import ru.sber.spring.filmlibrary.dto.FilmWithDirectorsDTO;
import ru.sber.spring.filmlibrary.mapper.DirectorMapper;

import ru.sber.spring.filmlibrary.mapper.DirectorWithFilmsMapper;
import ru.sber.spring.filmlibrary.model.Director;
import ru.sber.spring.filmlibrary.model.Film;
import ru.sber.spring.filmlibrary.repository.DirectorRepository;
import ru.sber.spring.filmlibrary.repository.FilmRepository;

import java.util.List;


@Service
public class DirectorService extends GenericService<Director, DirectorDTO> {
    private final FilmRepository filmRepository;
    private final DirectorRepository directorRepository;
    private final DirectorWithFilmsMapper directorWithFilmsMapper;

    protected DirectorService(DirectorRepository directorRepository,
                              DirectorMapper mapper,
                              FilmRepository filmRepository,
                              DirectorWithFilmsMapper directorWithFilmsMapper) {
        super(directorRepository, mapper);
        this.filmRepository = filmRepository;
        this.directorWithFilmsMapper = directorWithFilmsMapper;
        this.directorRepository = directorRepository;
    }

    public DirectorDTO addFilm(Long directorId, Long filmId) {
        Film film = filmRepository.findById(filmId)
                .orElseThrow(() -> new NotFoundException("Фильм с id: " + filmId + " не найден"));
        Director director = directorRepository.findById(directorId)
                .orElseThrow(() -> new NotFoundException("Директор с id: " + directorId + " не найден"));

        director.getFilms().add(film);
        return mapper.toDTO(repository.save(director));
    }
    public List<DirectorWithFilmsDTO> getAllDirectorsWithFilms() {
        return directorWithFilmsMapper.toDTOs(directorRepository.findAll());
    }

    
}
