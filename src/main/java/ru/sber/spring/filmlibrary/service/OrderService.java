package ru.sber.spring.filmlibrary.service;

import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import ru.sber.spring.filmlibrary.dto.OrderDTO;

import ru.sber.spring.filmlibrary.mapper.OrderMapper;
import ru.sber.spring.filmlibrary.model.Film;
import ru.sber.spring.filmlibrary.model.Order;

import ru.sber.spring.filmlibrary.model.User;
import ru.sber.spring.filmlibrary.repository.FilmRepository;
import ru.sber.spring.filmlibrary.repository.OrderRepository;
import ru.sber.spring.filmlibrary.repository.UserRepository;

import java.time.LocalDate;

@Service
public class OrderService extends GenericService<Order, OrderDTO> {
    private final UserRepository userRepository;
    private final FilmRepository filmRepository;

    protected OrderService(OrderRepository orderRepository, OrderMapper orderMapper,
                           UserRepository userRepository,FilmRepository filmRepository) {
        super(orderRepository, orderMapper);
        this.userRepository = userRepository;
       this.filmRepository = filmRepository;
    }
@Override
    public OrderDTO create(OrderDTO orderDTO) {
        User user = userRepository.findById(orderDTO.getUserId())
                .orElseThrow(() -> new NotFoundException("Пользователь с таким ID не найден"));
        Film film = filmRepository.findById(orderDTO.getFilmId())
                .orElseThrow(() -> new NotFoundException("Фильм с таким ID не найден"));
        Order order = new Order(); //!
        order.setFilm(film); //+
        order.setUser(user); //+
        if (orderDTO.getPurchase()) {
            order.setPurchase(true);
            order.setCreatedWhen(LocalDate.now().atStartOfDay());
        }
        else {
            order.setPurchase(false);
            order.setRentPeriod(orderDTO.getRentPeriod());
            order.setCreatedWhen(LocalDate.now().atStartOfDay());
            //дата создания заказа = дата начала аренды
        }

        repository.save(order);
        return mapper.toDTO(order);
    }


}
