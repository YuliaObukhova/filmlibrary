package ru.sber.spring.filmlibrary.REST.controler;


import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import ru.sber.spring.filmlibrary.dto.OrderDTO;
import ru.sber.spring.filmlibrary.model.Order;
import ru.sber.spring.filmlibrary.service.OrderService;

@RestController
@RequestMapping(value = "/orders")
@Tag(name = "Orders",
        description = "Контроллер для работы с заказами")
public class OrderController extends GenericController<Order, OrderDTO> {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        super(orderService);
        this.orderService = orderService;
    }
}
