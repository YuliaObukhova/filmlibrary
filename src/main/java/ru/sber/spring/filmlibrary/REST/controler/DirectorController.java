package ru.sber.spring.filmlibrary.REST.controler;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.sber.spring.filmlibrary.dto.DirectorDTO;
import ru.sber.spring.filmlibrary.model.Director;
import ru.sber.spring.filmlibrary.service.DirectorService;

@RestController
@RequestMapping(value = "/directors")
@Tag(name = "Directors",
        description = "Контроллер для работы с руководителями фильмотеки")
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DirectorController extends GenericController<Director, DirectorDTO> {

    private final DirectorService directorService;

    public DirectorController(DirectorService directorService) {
        super(directorService);
        this.directorService = directorService;
    }
//        @Secured(value = "ROLE_ADMIN")
//    public void test() {
//
//    }


    @Operation(description = "Добавить фильм к режиссёру", method = "addFilm")
    @PostMapping(value = "/addFilm", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDTO> addFilm(@RequestParam(value = "directorId") Long directorId,
                                               @RequestParam(value = "filmId") Long filmId) {
        return ResponseEntity.status(HttpStatus.OK).body(directorService.addFilm(directorId, filmId));
    }
}