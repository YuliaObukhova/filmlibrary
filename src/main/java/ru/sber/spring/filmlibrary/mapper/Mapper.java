package ru.sber.spring.filmlibrary.mapper;

import ru.sber.spring.filmlibrary.dto.GenericDTO;
import ru.sber.spring.filmlibrary.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {
    E toEntity(D dto);
    D toDTO(E entity);
    List<E> toEntities(List<D> dtos);
    List<D> toDTOs(List<E> entities);
}
