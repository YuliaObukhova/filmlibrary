package ru.sber.spring.filmlibrary.dto;

import lombok.*;
import ru.sber.spring.filmlibrary.model.Genre;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class FilmDTO
        extends GenericDTO {
    private String title;
    private int premierYear;
    private String country;
    private Genre genre;
    private Set<Long> directorsIds;
}
