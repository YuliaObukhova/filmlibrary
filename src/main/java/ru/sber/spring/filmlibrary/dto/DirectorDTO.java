package ru.sber.spring.filmlibrary.dto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class DirectorDTO extends GenericDTO {
    private String directorsFIO;
    private String position;
    private Set<Long> filmsIds;
}
