package ru.sber.spring.filmlibrary.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderDTO extends GenericDTO{
    private Integer rentPeriod;
    private Boolean purchase;
    private Long filmId;
    private Long userId;
}
