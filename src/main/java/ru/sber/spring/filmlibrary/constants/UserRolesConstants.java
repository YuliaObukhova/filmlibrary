package ru.sber.spring.filmlibrary.constants;

public interface UserRolesConstants {
    String ADMIN = "ADMIN";
    String MANAGER = "MANAGER";
    String USER = "USER";
}

