package ru.sber.spring.filmlibrary;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


@SpringBootApplication
public class FilmLibraryApplication {

    public static void main(String[] args) {
        SpringApplication.run(FilmLibraryApplication.class, args);

        String url = "jdbc:postgresql://localhost:5432/FilmLibrary";
        String username = "postgres";
        String password = "12345";
        System.out.println("Connecting...");

        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            System.out.println("Connection successful!");
        } catch (SQLException e) {
            System.out.println("Connection failed!");
            e.printStackTrace();
        }
    }
}