package ru.sber.spring.filmlibrary.config;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@SecurityScheme(
        name = "Bearer Authentication",
        type = SecuritySchemeType.HTTP,
        bearerFormat = "JWT",
        scheme = "bearer"
)
public class OpenApiConfig {
    //http://localhost:9090/api/rest/swagger-ui/index.html

    @Bean
    public OpenAPI filmlibraryProject() {
        return new OpenAPI()
                .info(new Info()
                .title("Фильмотека")
                .description("Сервис, позволяющий смотреть фильмы.")
                .version("v0.1")
                .license(new License().name("Appache 2.0").url("https://www.java.com/ru/"))
                .contact(new Contact().name("Yulia Obukhova")
                        .email("yuliaobukhova0306@gmail.com")
                        .url(""))
                );
    }
}
