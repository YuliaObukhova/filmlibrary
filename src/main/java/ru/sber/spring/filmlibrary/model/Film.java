package ru.sber.spring.filmlibrary.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.Set;

@Entity
@Table(name = "films")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_gen",
        sequenceName = "films_seq",
        allocationSize = 1)
public class Film extends GenericModel {

    @Column(name = "title", nullable = false)
    private String title;
    @Column(name = "premier_year")
    private Integer premierYear;
    @Column(name = "country")
    private String country;
    @Column(name = "genre", nullable = false)
    @Enumerated
    private Genre genre;


    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "films_directors",
            joinColumns = @JoinColumn(name = "film_id"), foreignKey = @ForeignKey(name = "FK_FILMS_DIRECTORS"),
            inverseJoinColumns = @JoinColumn(name = "director_id"), inverseForeignKey = @ForeignKey(name = "FK_DIRECTORS_FILMS"))

    private Set<Director> directors;

    @OneToMany(mappedBy = "film", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<Order> orders;
}
