package ru.sber.spring.filmlibrary.model;

public enum Genre {
    ROMANCE_COMEDY("Романтическая комедия"),
    SCIENCE_FICTION("Научная фантастика"),
    HORROR("Фильм ужасов"),
    DOCUMENTARY("Документальный фильм"),
    ANIMATED("Анимационный фильм"),
    ACTION("Боевик"),
    DRAMA("Драматический фильм"),
    COMEDY("Комедия"),
    ADVENTURE("Фильм о приключениях"),
    THRILLER("Триллер");

    private final String genreTextDisplay;

    Genre(String text) {
        this.genreTextDisplay = text;
    }

    public String getGenreTextDisplay() {
        return this.genreTextDisplay;
    }
}
