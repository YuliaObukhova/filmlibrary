package ru.sber.spring.filmlibrary.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "directors")
@SequenceGenerator(name = "default_gen",
        sequenceName = "directors_seq",
        allocationSize = 1)
//@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class,
// property = "@json_id")

public class Director extends GenericModel {
    @Column(name = "directors_fio", nullable = false)
    private String directorsFIO;
    @Column(name = "position")
    private String position;

@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
//@JsonIgnore
@JoinTable(name = "films_directors",
joinColumns =  @JoinColumn(name = "director_id"), foreignKey = @ForeignKey(name = "FK_DIRECTORS_FILMS"),
inverseJoinColumns = @JoinColumn(name = "film_id"), inverseForeignKey = @ForeignKey(name = "FK_FILMS_DIRECTORS"))
    private Set<Film> films;

}
