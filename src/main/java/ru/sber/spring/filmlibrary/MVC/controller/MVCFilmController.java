package ru.sber.spring.filmlibrary.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.sber.spring.filmlibrary.dto.FilmDTO;
import ru.sber.spring.filmlibrary.dto.FilmWithDirectorsDTO;
import ru.sber.spring.filmlibrary.service.FilmService;

import java.util.List;

@Controller
@Hidden
@RequestMapping("/films")
public class MVCFilmController {
    private final FilmService filmService;
    public MVCFilmController(FilmService filmService){
        this.filmService = filmService;
    }

    @GetMapping("")
    public String getAll(Model model){
        List<FilmWithDirectorsDTO> result = filmService.getAllFilmsWithDirectors();
        model.addAttribute("films", result);
        return "films/viewAllFilms";
    }

    //Нарисует форму создания книги
    @GetMapping("/add")
    public String create(){
        return "films/addFilm";
    }

    //Примет данные о создаваемой книге и создаст в БД
    //Потом вернёт нас на страницусо всеми книгами
    @PostMapping("/add")
    public String create(@ModelAttribute("filmForm") FilmDTO filmDTO){
        filmService.create(filmDTO);
        return "redirect:/films";
    }
}
