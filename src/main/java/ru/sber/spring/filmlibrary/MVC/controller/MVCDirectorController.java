package ru.sber.spring.filmlibrary.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.sber.spring.filmlibrary.dto.DirectorDTO;
import ru.sber.spring.filmlibrary.dto.DirectorWithFilmsDTO;
import ru.sber.spring.filmlibrary.service.DirectorService;

import java.util.List;

@Controller
@Hidden
@RequestMapping("directors")
public class MVCDirectorController {
private final DirectorService directorService;

    public MVCDirectorController(DirectorService directorService){
        this.directorService = directorService;
    }

    @GetMapping("")
    public String getAll(Model model){
        List<DirectorWithFilmsDTO> directorDTOList = directorService.getAllDirectorsWithFilms();
        model.addAttribute("directors", directorDTOList);
        return "directors/viewAllDirectors";
    }

    //Нарисует форму создания книги
    @GetMapping("/add")
    public String create(){
        return "directors/addDirector";
    }

    //Примет данные о создаваемой книге и создаст в БД
    //Потом вернёт нас на страницусо всеми книгами
    @PostMapping("/add")
    public String create(@ModelAttribute("directorForm") DirectorDTO directorDTO){
        directorService.create(directorDTO);
        return "redirect:/directors";
    }
}
