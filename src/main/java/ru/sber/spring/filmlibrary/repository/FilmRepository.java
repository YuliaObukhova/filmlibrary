package ru.sber.spring.filmlibrary.repository;

import org.springframework.stereotype.Repository;
import ru.sber.spring.filmlibrary.model.Film;
@Repository
public interface FilmRepository extends GenericRepository<Film> {
}
