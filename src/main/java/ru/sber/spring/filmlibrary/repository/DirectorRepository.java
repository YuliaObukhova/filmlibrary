package ru.sber.spring.filmlibrary.repository;

import org.springframework.stereotype.Repository;
import ru.sber.spring.filmlibrary.model.Director;

@Repository
public interface DirectorRepository extends GenericRepository<Director> {
}
