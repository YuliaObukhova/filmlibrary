package ru.sber.spring.filmlibrary.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.sber.spring.filmlibrary.model.User;

@Repository
public interface UserRepository extends GenericRepository<User> {

    User findUserByLogin(String login);
    User findUserByEmail(String email);
}

