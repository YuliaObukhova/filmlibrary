package ru.sber.spring.filmlibrary.repository;

import org.springframework.stereotype.Repository;
import ru.sber.spring.filmlibrary.model.Order;

@Repository
public interface OrderRepository
        extends GenericRepository<Order> {
}

